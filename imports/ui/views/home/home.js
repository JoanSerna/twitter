import angular from 'angular';
import angularMeteor from 'angular-meteor';
import uiRouter from '@uirouter/angularjs';

import template from './home.html';
import './home.less';

import { name as DocList } from '/imports/ui/components/common/afkarDocList/afkarDocList';
import {name as twiterLateral} from '/imports/ui/components/common/twiterLateral/twiterLateral';
import { name as AfkarProfileCard } from '/imports/ui/components/common/afkarProfileCard/afkarProfileCard';
import {name as AfkarViewPost} from '/imports/ui/components/common/afkarViewPost/afkarViewPost'
import {name as TwitterNav} from '/imports/ui/components/common/twitterNav/twitterNav'
import {name as TwitterPost} from '/imports/ui/components/common/twitterPost/twitterPost'

class Home {
    constructor( $reactive, $scope ) {
        $reactive( this ).attach( $scope );
        this.date = new Date();
        console.log( this.date );
        this.offset = this.date.getTimezoneOffset();
        this.timezone = Intl.DateTimeFormat().resolvedOptions().timeZone;
        console.log( Intl );
    }
}

const name = 'home';

export default angular.module( name, [
    angularMeteor,
    uiRouter,
    AfkarProfileCard,
    twiterLateral,
    AfkarViewPost,
    TwitterNav,
    TwitterPost
] )
    .component( name, {
        template : template,
        controller : [ '$reactive', '$scope', Home ],
        controllerAs : 'ctrl'
    } )
    .config( config );

function config( $stateProvider ) {
    'ngInject';
    $stateProvider
        .state( name, {
            url : '/afkar',
            template : '<home></home>'
        } );
}
