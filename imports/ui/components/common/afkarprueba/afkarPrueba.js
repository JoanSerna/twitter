import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './afkarPrueba.html';
import './afkarPrueba.less';



class afkarPrueba {
  constructor($reactive, $scope) {
    $reactive(this).attach($scope);

    this.helpers({
      afkar(){
      }

    })
  }
}

const name = 'afkarPrueba';

export default angular.module(name, [
  angularMeteor
  ])
.component(name, {
  template: template,
  controller: ['$reactive', '$scope', afkarPrueba],
  controllerAs: "ctrl"
});
