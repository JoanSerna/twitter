import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './twitterNav.html';
import './twitterNav.less';



class twitterNav {
  constructor($reactive, $scope) {
    $reactive(this).attach($scope);

    this.helpers({
      afkar(){
      }

    })
  }
}

const name = 'twitterNav';

export default angular.module(name, [
  angularMeteor
  ])
.component(name, {
  template: template,
  controller: ['$reactive', '$scope', twitterNav],
  controllerAs: "ctrl"
});
