import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './afkarViewPost.html';
import './afkarViewPost.less';


class AfkarViewPost {
    constructor($reactive, $scope) {
        $reactive(this).attach($scope);
        this.cards = [
            {
                title: 'Card 1',
                description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda at dicta dolore, eius est et facere iste laboriosam mollitia officia perferendis qui quod, quos reiciendis saepe, tenetur velit. Ea, nihil.'
            },
            {
                title: 'Card 2',
                description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda at dicta dolore, eius est et facere iste laboriosam mollitia officia perferendis qui quod, quos reiciendis saepe, tenetur velit. Ea, nihil.'
            },
            {
                title: 'Card 3',
                description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda at dicta dolore, eius est et facere iste laboriosam mollitia officia perferendis qui quod, quos reiciendis saepe, tenetur velit. Ea, nihil.'
            },
            {
                title: 'Card 4',
                description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda at dicta dolore, eius est et facere iste laboriosam mollitia officia perferendis qui quod, quos reiciendis saepe, tenetur velit. Ea, nihil.'
            },
            {
                title: 'Card 5',
                description: 'Lorem ipsum dolor sit amet, consectetur adipisicing elit. Assumenda at dicta dolore, eius est et facere iste laboriosam mollitia officia perferendis qui quod, quos reiciendis saepe, tenetur velit. Ea, nihil.'
            }
        ];


        this.helpers({
            afkar() {
            }

        });
    }
}

const name = 'afkarViewPost';

export default angular.module(name, [
    angularMeteor
])
    .component(name, {
        template: template,
        controller: ['$reactive', '$scope', AfkarViewPost],
        controllerAs: 'ctrl'
    });
