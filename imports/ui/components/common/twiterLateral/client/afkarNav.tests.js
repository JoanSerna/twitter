import angular from 'angular';
import 'angular-mocks';

import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';
import { sinon } from 'meteor/practicalmeteor:sinon';

import afkarNav from '/imports/ui/components/common/afkarNav/afkarNav';

describe('afkarNav', function() {
  var element;

  beforeEach(function() {
    var $compile;
    var $rootScope;
    window.module(afkarNav.name);

    inject(function(_$compile_, _$rootScope_){
      $compile = _$compile_;
      $rootScope = _$rootScope_;
    });

    element = $compile('<afkar-nav></afkar-nav>')($rootScope.$new(true));
    $rootScope.$digest();
  });

  describe('component', function() {
    it('should be showing Afkar in the nav', function() {
      assert.include(element[0].querySelector('span').innerHTML, 'Afkar');
    });
  });
})
