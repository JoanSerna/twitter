import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './twitterPost.html';
import './twitterPost.less';



class twitterPost {
  constructor($reactive, $scope) {
    $scope.imagePath = 'https://lorempixel.com/320/240';
    $reactive(this).attach($scope);

    this.helpers({
      afkar(){
      }

    })
  }
}

const name = 'twitterPost';

export default angular.module(name, [
  angularMeteor
  ])
.component(name, {
  template: template,
  controller: ['$reactive', '$scope', twitterPost],
  controllerAs: "ctrl"
});
