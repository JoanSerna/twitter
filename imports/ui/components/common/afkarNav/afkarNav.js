import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './afkarNav.html';
import './afkarNav.less';



class AfkarNav {
  constructor($reactive, $scope) {
    $reactive(this).attach($scope);

    this.helpers({
      afkar(){
      }

    })
  }
}

const name = 'afkarNav';

export default angular.module(name, [
  angularMeteor
  ])
.component(name, {
  template: template,
  controller: ['$reactive', '$scope', AfkarNav],
  controllerAs: "ctrl"
});
