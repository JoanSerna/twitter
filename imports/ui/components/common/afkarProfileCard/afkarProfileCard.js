import angular from 'angular';
import angularMeteor from 'angular-meteor';
import template from './afkarProfileCard.html';
import './afkarProfileCard.less';


class AfkarProfileCard {
    constructor( $reactive, $scope ) {
        $scope.imagePath = 'https://lorempixel.com/320/240';
        $scope.profileName = 'Joan Serna';
        $scope.profileUsername = '@joansernajs';

        $reactive( this ).attach( $scope );

        this.helpers( {
            afkar() {
            }

        } );
    }
}

const name = 'afkarProfileCard';

export default angular.module( name, [
    angularMeteor
] )
    .component( name, {
        template : template,
        controller : [ '$reactive', '$scope', AfkarProfileCard ],
        controllerAs : 'ctrl'
    } );
