import angular from 'angular';
import 'angular-mocks';

import { Meteor } from 'meteor/meteor';
import { assert } from 'meteor/practicalmeteor:chai';
import { sinon } from 'meteor/practicalmeteor:sinon';

import afkarDocList from '../afkarDocList';

describe('nav', function() {
  var element;

  beforeEach(function() {
    var $compile;
    var $rootScope;
    window.module(afkarDocList.name);

    inject(function(_$compile_, _$rootScope_){
      $compile = _$compile_;
      $rootScope = _$rootScope_;
    });

    element = $compile('<afkar-doc-list></afkar-doc-list>')($rootScope.$new(true));
    $rootScope.$digest();
  });

  describe('component', function() {
    it('should be showing Afkar Docs in the header', function() {
      assert.include(element[0].querySelector('md-subheader').innerHTML, 'Afkar Docs');
    });
  });
})
