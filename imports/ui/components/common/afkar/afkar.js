import angular from 'angular';
import angularMeteor from 'angular-meteor';
import ngMaterial from 'angular-material';
// import uiRouter from 'angular-ui-router';
import uiRouter from '@uirouter/angularjs';

import template from './afkar.html';
import { name as Nav } from '/imports/ui/components/common/afkarNav/afkarNav';
import { name as Home } from '/imports/ui/views/home/home';
import { name as TwitterNav } from '/imports/ui/components/common/twitterNav/twitterNav';

class Afkar {}

const name = 'afkar';

// create a module
export default angular.module(name, [
  angularMeteor,
  uiRouter,
  ngMaterial,
  Nav,
  Home,
  TwitterNav
]).component(name, {
  template: template,
  controllerAs: name,
  controller: Afkar
})
  .config(config);

function config($locationProvider, $urlRouterProvider, $mdThemingProvider) {
  'ngInject';

  $locationProvider.html5Mode(true);

  $urlRouterProvider.otherwise('/afkar');

  $mdThemingProvider.theme('default')
  .primaryPalette('grey')
  .accentPalette('pink');

}