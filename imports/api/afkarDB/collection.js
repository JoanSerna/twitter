import { Mongo } from 'meteor/mongo';

export const AfkarDB = new Mongo.Collection("afkarDB");

AfkarDB.allow({
  insert() {
    return true;
  },
  update() {
    return true;
  },
  remove() {
    return true;
  }
});