import { AfkarDB } from './collection';

function insertDocument(message){
	AfkarDB.insert({message: message});
}
function deleteDocument(message){
	AfkarDB.remove({message: message});
}



Meteor.methods({
	insertDocument,
	deleteDocument
});