import { Meteor } from 'meteor/meteor';
import { AfkarDB } from './collection.js';
import faker from 'faker';
// import { assert } from 'meteor/practicalmeteor:chai';
import '/imports/api/afkarDB';

if (Meteor.isServer) {
	describe('AfkarDB', () => {
		describe('methods', () => {
			const message = faker.lorem.sentence();

			beforeEach(() => {
				AfkarDB.remove({});
			});

			it('can insert a document with a message', () => {
				// Find the internal implementation of the document method so we can
        		// test it in isolation
        		const insertDocument = Meteor.server.method_handlers['insertDocument'];
        		// Set up a fake method invocation that looks like what the method expects
        		const invocation = {
        			message
        		};

      		 	 // Run the method with `this` set to the fake invocation
      		 	 insertDocument.apply(invocation, [message]);
      		 	 // Verify that the method does what we expected
      		 	 chai.assert.equal(AfkarDB.find().count(), 1);
      		 	 chai.assert.equal(AfkarDB.findOne().message, message)
      		 	});
			it('can delete a document by its message', () => {
				AfkarDB.insert({
					message: message
				})
      			// Find the internal implementation of the document method so we can
        		// test it in isolation
        		const deleteDocument = Meteor.server.method_handlers['deleteDocument'];
        		// Set up a fake method invocation that looks like what the method expects
        		const invocation = {
        			message
        		};

      	 		// Run the method with `this` set to the fake invocation
      	 		deleteDocument.apply(invocation, [message]);
      	 		// Verify that the method does what we expected
      	 		chai.assert.equal(AfkarDB.find().count(), 0);
      	 	});
		});
	});
}

